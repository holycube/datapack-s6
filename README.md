# L'officiel de la saison 6 de HolyCube

Voici le datapack qui est utilisé sur la saison 6 de HolyCube.

CE DATAPACK NE CONTIENT PAS LE HOLYWRENCH: celui-çi est disponible dans le mod qui n'est présentement pas distribué.

La liste des fonctionnalités incluses:
* Nouvelles recettes;
* Drop d'oeufs de dragon, double drop de shulker shells et anti-grief d'enderman.

## Installation
Tous les datapacks s'installent dans le dossier `world/datapacks/` d'une sauvegarde MineCraft. Cet article du wiki gamepedia de MineCraft vous aidera plus en profondeur: [Tutoriels/Installer un pack de données](https://minecraft-fr.gamepedia.com/Tutoriels/Installer_un_pack_de_donn%C3%A9es).

- Placez les fichiers `.zip` venant de l'onglet [Releases](https://gitlab.com/holycube/datapack-s6/-/releases) dans votre dossier datapacks dans votre monde MineCraft.

### Les nouvelles recettes
Une liste des recettes est disponible [ici](https://gitlab.com/holycube/datapack-s6/-/raw/main/crafts.png).
